import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'FirstFlowPages/HairtipsPage.dart';
import 'FirstFlowPages/HomeFragment.dart';
import 'FirstFlowPages/MyAppointmentsPage.dart';
import 'FirstFlowPages/StylistPage.dart';
import 'FirstFlowPages/AccountPage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // fixing app orientation.
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: new ThemeData(
          // brightness: Brightness.dark,
          primaryColor: const Color(0xFF364195),
          accentColor: const Color(0xFF364195),
          hintColor: const Color(0xFF364195),
          inputDecorationTheme: new InputDecorationTheme(
              labelStyle: new TextStyle(color: Color(0xFF364195)),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(4.0)))),
      home: new SplashPage(),
      routes: <String, WidgetBuilder>{
        '/PhoneNumberLogin': (BuildContext context) => new PhoneNumberLogin(),
        '/UserRegistrationPage': (BuildContext context) =>
            new UserRegistrationPage(),
        '/HomePage': (BuildContext context) => new HomePage()
      },
    );
  }
}

class SplashPage extends StatefulWidget {
  @override
  SplashPageState createState() => SplashPageState();
}

class SplashPageState extends State<SplashPage> {
  void navigationToNextPage() {
    Navigator.pushNamed(context, '/PhoneNumberLogin');
  }

  startSplashScreenTimer() async {
    var _duration = new Duration(seconds: 5);
    return new Timer(_duration, navigationToNextPage);
  }

  @override
  void initState() {
    super.initState();
    startSplashScreenTimer();
  }

  @override
  Widget build(BuildContext context) {
    // To make this screen full screen.
    // It will hide status bar and notch.
    SystemChrome.setEnabledSystemUIOverlays([]);

    // full screen image for splash screen.
    return Container(
        child: new Image.asset('assets/splash.png', fit: BoxFit.fill));
  }
}

class PhoneNumberLogin extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _PhoneNumberLoginState();
}

class _PhoneNumberLoginState extends State<PhoneNumberLogin>
    with SingleTickerProviderStateMixin {
  static final formKey = new GlobalKey<FormState>();

  AnimationController _iconAnimationController;
  Animation<double> _iconAnimation;

  String strPhonumber, strSmsCode, strVerificationId;

  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      print('phone number: $strPhonumber');
      return true;
    }
    return false;
  }

  @override
  void initState() {
    super.initState();
    _iconAnimationController = new AnimationController(
        vsync: this, duration: new Duration(milliseconds: 500));
    _iconAnimation = new CurvedAnimation(
      parent: _iconAnimationController,
      curve: Curves.bounceOut,
    );
    _iconAnimation.addListener(() => this.setState(() {}));
    _iconAnimationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      body: new Stack(key: formKey, fit: StackFit.expand, children: <Widget>[
        new Image(
          image: new AssetImage("assets/splash.png"),
          fit: BoxFit.fill,
          // colorBlendMode: BlendMode.darken,
          // color: Colors.black87,
        ),
        new Theme(
          data: new ThemeData(
              brightness: Brightness.dark,
              inputDecorationTheme: new InputDecorationTheme(
                hintStyle: new TextStyle(
                    color: const Color(0xFFEAEAEA), fontSize: 27.0),
                labelStyle: new TextStyle(
                    color: const Color(0xFFEAEAEA), fontSize: 27.0),
              )),
          isMaterialAppTheme: true,
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Image(
                image: new AssetImage("assets/demodklogosplash.png"),
                width: 200.0,
                height: 200.0,
              ),
              new Container(
                padding: const EdgeInsets.all(40.0),
                child: new Form(
                  autovalidate: true,
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      new TextFormField(
                        decoration: new InputDecoration(
                            labelText: "Mobile Number",
                            fillColor: const Color(0xFFEAEAEA)),
                        keyboardType: TextInputType.number,
                        validator: (value) => value.isEmpty
                            ? 'Mobile number can\'t be empty.'
                            : null,
                        onSaved: (val) => strPhonumber = val,
                      ),
                      new Padding(
                        padding: const EdgeInsets.only(top: 60.0),
                      ),
                      new MaterialButton(
                          height: 46.0,
                          minWidth: 300.0,
                          color: const Color(0xFF364195),
                          splashColor: const Color(0xFF5867DD),
                          // splashColor: Colors.teal,
                          // textColor: Colors.white,
                          // child: new Icon(FontAwesomeIcons.signInAlt),
                          // onPressed: validateAndSave,
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        UserRegistrationPage()));
                          },
                          child: Text('Log In',
                              style: TextStyle(color: Colors.white)),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0)))
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ]),
    );
  }

  Widget loginAnimation() {
    var buttonSqueezeAnimation;
    return Container(
        width: buttonSqueezeAnimation.value,
        height: 60.0,
        alignment: FractionalOffset.center,
        decoration: new BoxDecoration(
            color: const Color.fromRGBO(247, 64, 106, 1.0),
            borderRadius: new BorderRadius.all(const Radius.circular(30.0))),
        child: buttonSqueezeAnimation.value > 75.0
            ? new Text(
                "Sign In",
                style: new TextStyle(
                  color: Colors.white,
                  fontSize: 20.0,
                  fontWeight: FontWeight.w300,
                  letterSpacing: 0.3,
                ),
              )
            : new CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
              ));
  }
}

class UserRegistrationPage extends StatefulWidget {
  @override
  UserRegistrationPageState createState() => UserRegistrationPageState();
}

class UserRegistrationPageState extends State<UserRegistrationPage> {
  GlobalKey<FormState> _globalKey = new GlobalKey();
  bool validate = false;

  String strPhoneNumber, strFirstname, strLastname, strDob, strGender;

  Color randomColor() => Color(0xFFFFFF).withOpacity(1.0);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: new Scaffold(
        body: new SingleChildScrollView(
          child: new Container(
            child: new Form(
              key: _globalKey,
              autovalidate: validate,
              child: formUi(),
            ),
          ),
        ),
      ),
    );
  }

  Widget formUi() {
    return new Column(
      children: <Widget>[
        new Image(
          image: new AssetImage("assets/bgtopbar.png"),
        ),
        allTextFieldWithMargin()
      ],
    );
  }

  Widget allTextFieldWithMargin() {
    return Container(
      margin: new EdgeInsets.all(30.0), // Or set whatever you want
      child: Column(
        children: <Widget>[
          new TextFormField(
            //  margin: new EdgeInsets.all(15.0),
            decoration: new InputDecoration(hintText: 'Mobile Number'),
            keyboardType: TextInputType.phone,
            maxLength: 10,
            validator: validateMobilenumber,
            onSaved: (String val) {
              strPhoneNumber = val;
            },
          ),
          new TextFormField(
            decoration: new InputDecoration(hintText: 'First Name'),
            keyboardType: TextInputType.emailAddress,
            maxLength: 20,
            validator: validateFirstname,
            onSaved: (String val) {
              strFirstname = val;
            },
          ),
          new TextFormField(
            decoration: new InputDecoration(hintText: 'Last Name'),
            maxLength: 20,
            validator: validateLastname,
            onSaved: (String val) {
              strLastname = val;
            },
          ),
          new TextFormField(
            decoration: new InputDecoration(hintText: 'Gender'),
            maxLength: 10,
            validator: validateGender,
            onSaved: (String val) {
              strGender = val;
            },
          ),
          new TextFormField(
            decoration: new InputDecoration(hintText: 'DOB'),
            maxLength: 10,
            validator: validateDob,
            onSaved: (String val) {
              strDob = val;
            },
          ),
          new SizedBox(height: 20.0),
          new RaisedButton(
            onPressed: saveToDatabase,
            child: new Text('Complete'),
          )
        ],
      ),
    );
  }

  String validateFirstname(String value) {
    String pattern = r'(^[a-zA-Z $]*)';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return 'First Name is required';
    } else if (!regExp.hasMatch(value)) {
      return "Name must be a-z and A-Z";
    }
    return null;
  }

  String validateLastname(String value) {
    String pattern = r'(^[a-zA-Z $]*)';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return 'Last Name is required';
    } else if (!regExp.hasMatch(value)) {
      return "Name must be a-z and A-Z";
    }
    return null;
  }

  String validateGender(String value) {
    String pattern = r'(^[a-zA-Z $]*)';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return 'Gender is required';
    } else if (!regExp.hasMatch(value)) {
      return "Name must be a-z and A-Z";
    }
    return null;
  }

  String validateDob(String value) {
    if (value.length == 0) {
      return 'Dob is required';
    } else {
      return null;
    }
  }

  String validateMobilenumber(String value) {
    String pattern = r'(^[0-9+]*)';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return 'MobileNumber is required';
    } else if (!regExp.hasMatch(value)) {
      return "MobileNumber must Digits";
    }
    return null;
  }

  saveToDatabase() {
    _globalKey.currentState.validate();
    if (_globalKey.currentState.validate()) {
      _globalKey.currentState.save();
      print("phonenumber $strPhoneNumber");
      print("firstname $strFirstname");
      print("lastname $strLastname");
      print("gender $strGender");
      print("dob $strDob");
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => HomePage()));
    } else {
      setState(() {
        validate = true;
      });
    }
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;
  List<Widget> firstFlowpagesList = List();
  static String accountname = 'Aravind Vemula';
  static String email = 'vemula.aravind336@gmail.com';
  static String acc = 'AV';

  final bottomNavigationColor = const Color(0xFF364195);
  Drawer drawer = new Drawer(
      child: new Column(children: <Widget>[
    new UserAccountsDrawerHeader(
      accountName: Text(accountname),
      accountEmail: Text(email),
      currentAccountPicture:
          new CircleAvatar(backgroundColor: Colors.brown, child: new Text(acc)),
    )
  ]));

  @override
  void initState() {
    firstFlowpagesList
      ..add(HomeFragment())
      ..add(StylistPage())
      ..add(MyAppointmentsPage())
      ..add(HairtipsPage())
      ..add(AccountPage());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFF000000),
        title: new Image.asset('assets/demodklogosplash.png', fit: BoxFit.fill),
        centerTitle: true,
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Text('Drawer Header'),
              decoration: BoxDecoration(color: Colors.blue),
            ),
            ListTile(
              title: Text('Home'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('Products'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('Promotions'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('Purchases'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('Settings'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
      body: firstFlowpagesList[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.shifting,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.home, color: bottomNavigationColor),
              title:
                  Text('Home', style: TextStyle(color: bottomNavigationColor))),
          BottomNavigationBarItem(
              icon: Icon(Icons.business, color: bottomNavigationColor),
              title: Text('Business',
                  style: TextStyle(color: bottomNavigationColor))),
          BottomNavigationBarItem(
              icon: Icon(Icons.school, color: bottomNavigationColor),
              title: Text('School',
                  style: TextStyle(color: bottomNavigationColor))),
          BottomNavigationBarItem(
              icon: Icon(Icons.ac_unit, color: bottomNavigationColor),
              title: Text('Haitips',
                  style: TextStyle(color: bottomNavigationColor))),
          BottomNavigationBarItem(
              icon: Icon(Icons.access_time, color: bottomNavigationColor),
              title: Text('Account',
                  style: TextStyle(color: bottomNavigationColor))),
        ],
        currentIndex: _selectedIndex,
        fixedColor: const Color(0xFF364195),
        onTap: _onItemTapped,
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}