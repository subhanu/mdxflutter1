import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:meetdaxdemodk/FirstFlowPages/StylistPage.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:meetdaxdemodk/DataModel/ProductsCollection.dart';
import 'package:meetdaxdemodk/DataModel/RaisedGradientButton.dart';

class HomeFragment extends StatefulWidget {
  @override
  _HomeFragmentState createState() => _HomeFragmentState();
}

class _HomeFragmentState extends State<HomeFragment> {
  final double dividerHeight = 2.0;
  List<ProductsCollection> productData = []
    ..add(ProductsCollection(
        'Discipline curl', 'sjkhdjfhsjdhjdh', 20, 'akhsgdahghsgdh'))
    ..add(ProductsCollection(
        'Discipline curl', 'sjkhdjfhsjdhjdh', 20, 'akhsgdahghsgdh'))
    ..add(ProductsCollection(
        'Discipline curl', 'sjkhdjfhsjdhjdh', 20, 'akhsgdahghsgdh'));

  GoogleMapController mapController;
  final LatLng _center = const LatLng(45.521563, -122.677433);

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: new Scaffold(
          body: new SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                buildBody(),
                productsCard(),
                new SizedBox(height: 10.0),
                new Text(
                  'OPENING HOURS',
                  style: TextStyle(
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.w400,
                      fontSize: 16.0,
                      color: const Color(0xFF5867DD)),
                ),
                new Divider(
                  color: Color(0xFF8E8E93),
                ),
                new SizedBox(height: 8.0),
                new Text(
                  'M O N D A Y - F R I D A Y   9:00 - 17:30',
                  style: TextStyle(
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.bold,
                      fontSize: 15.0,
                      color: const Color(0xFF535353)),
                ),
                new SizedBox(height: 8.0),
                new Text(
                  '                S A T U R D A Y   8:00 - 12:00',
                  style: TextStyle(
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.bold,
                      fontSize: 15.0,
                      color: const Color(0xFF535353)),
                ),
                new SizedBox(height: 8.0),
                new Text(
                  '               S U  N D A Y   CLOSED',
                  style: TextStyle(
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.bold,
                      fontSize: 15.0,
                      color: const Color(0xFF535353)),
                ),
                new SizedBox(height: 8.0),
                new Divider(
                  height: dividerHeight,
                  color: Color(0xFF8E8E93),
                ),
                new SizedBox(height: 20.0),
                new Text(
                  'FOR DISCOUNTS/ANY QUESTIONS',
                  style: TextStyle(
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.w400,
                      fontSize: 16.0,
                      color: const Color(0xFF5867DD)),
                ),
                new SizedBox(height: 20.0),
                new RaisedGradientButton(
                  height: 46.0,
                  width: 250.0,
                  onPressed: () => launch("tel:+45 7443 2020"),
                  child: new Text("ring +45 7443 2020",
                      style: TextStyle(fontSize: 20.0, color: Colors.white)),
                ),
                new SizedBox(height: 20.0),
                new Text(
                  'SERVICES  &  PRICELIST',
                  style: TextStyle(
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.w400,
                      fontSize: 16.0,
                      color: const Color(0xFF5867DD)),
                ),
                new Divider(
                  color: Color(0xFF8E8E93),
                ),
            
            new GoogleMap(
              onMapCreated: _onMapCreated,
              options: GoogleMapOptions(
                cameraPosition: CameraPosition(
                  target:_center,
                )
              ),
            )
              ],
            ),
          ),
        ));
  }

  Widget buildBody() {
    return Stack(
        // alignment: Alignment.bottomCenter,
        children: <Widget>[
          new Image(
            image: new AssetImage('assets/homebg.png'),
          ),
          Positioned(
            left: 50.0,
            right: 50.0,
            bottom: 40.0,
            height: 64.0, // adjust this if you want some padding
            child: RaisedGradientButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => StylistPage()));
              },
              child: new Text(
                "BOOK AN APPOINTMENT",
                style: TextStyle(fontSize: 20.0, color: Colors.white),
              ),
              gradient: LinearGradient(
                colors: <Color>[
                  const Color(0xFF000000),
                  const Color(0xFF000000),
                  const Color(0xFF40079B)
                ],
              ),
            ), // child widget
          ),
        ]);
  }

  Widget productsCard() {
    return Container(
        decoration: new BoxDecoration(color: const Color(0xFFEAEAEA)),
        height: 150.57,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: productData.length,
          itemBuilder: (BuildContext context, int i) => Card(
                child: new Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Container(
                    width: 257.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        new Image(
                          height: 119,
                          width: 95,
                          image: AssetImage('assets/products_2.png'),
                        ),
                        productDetails(i),
                      ],
                    ),
                  ),
                ),
              ),
        ));
  }

  Widget productDetails(int index) {
    return Container(
      child: new Padding(
        padding: const EdgeInsets.all(0.0),
        child: Column(
          // crossAxisAlignment: CrossAxisAlignment.center,
          // mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,

          children: <Widget>[
            Text(productData[index].description,
                style: TextStyle(
                    fontSize: 15.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.black)),
            Text(productData[index].name,
                textAlign: TextAlign.start,
                style:
                    TextStyle(fontSize: 13.0, color: const Color(0xFF535353))),
            Text(productData[index].price.toString(),
                style: TextStyle(fontSize: 17.0, color: Colors.black))
          ],
        ),
      ),
    );
  }
}