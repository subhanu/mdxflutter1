class ProductsCollection{
  final String name;
  final String description;
  final int price;
  final String imageUrl;

  ProductsCollection(this.name, this.description, this.price,this.imageUrl);
}
